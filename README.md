# PRACTICA 6 #

En entra práctica vamos a comprobar el uso de las funciones de agregación multidimensionales(rollup, cube), pivatado de datos con pivot y la funciones de ventana que ofrece spark.

### 1. Creamos Estructura para la práctica 6 en nuestro workSpace###

* **Package**: Para crear el package utilizaremos el asistente de intellij, sobre la carpeta /src/main/scala, botón derecho-New-Package el nombre del ***bluetab.courses.spark.practica6***
* **AppLoans**: Como en el punto anterior creamos una nueva clase, selectionamos tipo object y al llamaremos ***AppLoans***
* **Loans**: Crearemos un trait donde guardaremos la funciones para las transformaciones. La creación similar al punto 2


### 2. Clase principal el proceso ###

```scala
package bluetab.courses.spark.practica6

import bluetab.courses.spark.practica3.SparkApp

object LoansApp extends SparkApp with Loans {

  loadTablon

}

}
```


### 3. Template de  ***Loan*** ###

Para realizar la práctica partiremos de la siguiente plantilla :

```scala
package bluetab.courses.spark.practica6

import bluetab.courses.spark.practica3.Constants
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

trait Loans extends Constants {

    def readMasterTable(table: String)(implicit spark: SparkSession): DataFrame = {
      spark.read.table(table)
    }

    def loadTablon(implicit spark: SparkSession) = {

      val df  = readMasterTable(MTLoans)

        df.printSchema()

      // 1. Totales de loans, totales de loans por año, totales loans año y duración (Rollup)
    


      // 2. Total Amount por años para cada uno de los status, los años serán las columnas (Pivot)
    

      // 3. Windows - Ranking de los 10 payment mayores de cada año
    


      // 4. Diferencia de la suma de los "amount" entre años por status
    
    }
}


```

# EJERCICIOS 6 #

## 1. Salida de informe de transacciones ##
* Las transacciones con opeation=null se asignarán como no definidas "Undefined"
* Salida total de amount y varianza(amount) para las transacciones agregadas por:
      * Total
      * Por año
      * Por año y Operacion
      * Por operacion
* Orden ascendente con valores nulos primero por año y operacion
* Las columnas total y varianza tienen que ser decimal(20,2)
* Fichero de salida csv con cabecera y con separador de campo ";"
* Nombre del fichero TotalTransaciones.csv


Sample:
                                        
|year|     operation|         total|    varianza|
|----|--------------|--------------|------------|
|null|          null|12515587120.60| 90682446.06|
|null|PREVOD NA UCET| 1345275572.60|  7393038.99|
|null| PREVOD Z UCTU| 1562959906.00|200043355.00|
|null|     Undefined|   54941379.00|     6481.95|
|null|         VKLAD| 4837043564.00|144986727.03|
|null|         VYBER| 4679025899.00| 78297002.60|
|null|  VYBER KARTOU|   36340800.00|  1472661.99|
|2013|          null|  408617057.40|118045594.66|
|2013|PREVOD NA UCET|   20318821.40|  7385395.72|



## 2. Toltal balance anual Transaciones por tipo ##
* Total de balance anual de las transaciones por tipo, los valores tipos pasan a ser columnas
* Los balances tienen ser tipo decimal(20,2)
* Salida fichero .json
* Nombre del fichero TotalBalanceAnualTransaciones.csv

Sample:

|year|        PRIJEM|       VYBER|         VYDAJ|
|----|--------------|------------|--------------|
|2018|10642115419.80|514020976.40|15166856061.60|
|2015| 3974407338.00|175747861.80| 5533560900.00|
|2013| 1072429425.80| 32303738.60|  941041638.60|
|2014| 2753705230.40|122592967.00| 3681014212.80|
|2016| 6116692355.60|250618399.80| 8299309561.40|
|2017| 9027295490.80|414312871.00|12657341938.60|



## 3. Informe loans - Definición UDFs  ##

En este ejecicio vamos a crear tres udfs que nos van a uyudar a generar el informe de los loans.
El fichero de salida será "informeloans2018.cvs" con formato cvs, separador ";" y he incluirá la cabecera.

### UDF-Capital Amortizado ### 

```
((pagados * payments)/amount)*100
```
            
```scala
val fCapAmort = (pagados:Int, payments:Double, amount:Double) =>???
```

### UDFF-Capital Vivo ###
```
amount - (payments * pagados)
```
```scala
val fCapVivo = (pagados:Int, payments:Double, amount:Double) =>???
```

### UDF-Mask Account ###
Enmascara los identificadores de cuenta 
```
 Muestra el primer dígito seguido ### y los dos últimos dígitos. Ej (1###56)
 Si la cuenta es de 3 digitos o menor, sólo se muestra el primero seguido de ###. Ej(1###)
```
```scala
val fMask = (account:Int) => ???
```

### Notas ###
Columna ***pagados***: Número de meses que han transcurrido desde el inicio del prestamo(date) hasta la fecha actual, que supondremos que es '2018-12-01'.
Para calcular esta columna tenemos que utilizar la función ***months_between*** proporcionada por spark en el paquete [functions](https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.functions$)

### Sample ###

|loan_id|account_mask|  amount|duration|pagados|payments|Capital Amortizado(%)|Capital - Vivo|
|-------|------------|--------|--------|-------|--------|---------------------|--------------|
|   5170|      1###71|25320.00|      60|     58|  422.00|                96.67|        844.00|
|   6087|      5###13|30066.00|      60|     57|  501.10|                95.00|       1503.30|
|   7055|      1###79|16710.00|      60|     55|  278.50|                91.67|       1392.50|
|   6103|      5###85|14934.00|      60|     55|  248.90|                91.67|       1244.50|
|   6696|      8###21| 8904.00|      60|     54|  148.40|                90.00|        890.40|
|   5568|      2###33|27252.00|      60|     54|  454.20|                90.00|       2725.20|
|   6736|      8###58|28836.00|      60|     53|  480.60|                88.33|       3364.20|
|   5319|      1###11|23946.00|      60|     53|  399.10|                88.33|       2793.70|
|   5837|      4###60|24456.00|      60|     53|  407.60|                88.33|       2853.20|
|   5549|      2###24| 5046.00|      60|     52|   84.10|                86.67|        672.80|
